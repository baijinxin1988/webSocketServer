package com.loforce.websocketserver.common.base;

import net.sf.ehcache.store.chm.ConcurrentHashMap;

import java.util.Map;

/**
 * 全局常量
 * Created by shuzheng on 2017/2/18.
 */
public class BaseConstants {
    /** 当前用户 */
    public static final String CURRENT_USER = "CURRENT_USER";
    /** 所有当前登录用户缓存集合 */
    public static final Map<String,Object> ALL_CURRENT_USERS = new ConcurrentHashMap();

}
