package com.loforce.websocketserver.common;

/**
 * 结果格式工具类
 *
 * @author zhangbin.
 * @date 2017/12/15.
 */
public class JsonResult<T> {
    private T status;
    private String msg;
    private T data;

    public JsonResult(T status, String msg, T data) {
        this.status = status;
        this.msg = msg;
        this.data = data;
    }

    public JsonResult(T status, String msg) {
        this.status = status;
        this.msg = msg;
    }

    public JsonResult() {
    }

    public T getStatus() {
        return status;
    }

    public void setStatus(T status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
