package com.loforce.websocketserver.client;

import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EndpointConfig;

/**
 * 解码器的实现类
 *
 * @author zhangbin.
 * @date 2017/12/17.
 */
public class SampleDecoder implements Decoder.Text<String> {

    public void init(EndpointConfig paramEndpointConfig) {
        // Auto-generated method stub
    }

    public void destroy() {
        // Auto-generated method stub
    }

    public String decode(String paramString) throws DecodeException {
        // Auto-generated method stub
        return paramString.toLowerCase();
    }

    public boolean willDecode(String paramString) {
        // Auto-generated method stub
        return true;
    }

}