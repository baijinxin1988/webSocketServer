package com.loforce.websocketserver.client;

import javax.websocket.EncodeException;
import javax.websocket.EndpointConfig;

/**
 * 编码器的实现类
 *
 * @author zhangbin.
 * @date 2017/12/17.
 */
public class SimpleEncoder implements javax.websocket.Encoder.Text<String> {

    public void init(EndpointConfig paramEndpointConfig) {
        //Auto-generated method stub
        System.out.println("Encoder init: " + paramEndpointConfig.getUserProperties());
    }

    public void destroy() {
        //Auto-generated method stub
    }

    public String encode(String paramT) throws EncodeException {
        //Auto-generated method stub
        return paramT.toUpperCase();
    }

}
