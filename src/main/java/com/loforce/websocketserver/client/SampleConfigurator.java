package com.loforce.websocketserver.client;

import javax.websocket.ClientEndpointConfig;
import javax.websocket.HandshakeResponse;
import java.util.List;
import java.util.Map;

/**
 * 配置的实现类
 *
 * @author zhangbin.
 * @date 2017/12/17.
 */
public class SampleConfigurator extends ClientEndpointConfig.Configurator {

    @Override
    public void beforeRequest(Map<String, List<String>> headers) {
        //Auto-generated method stub
        System.out.println(headers);
    }

    @Override
    public void afterResponse(HandshakeResponse handshakeResponse) {
        //Auto-generated method stub
        System.out.println(handshakeResponse);
    }

}
