package com.loforce.websocketserver.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.websocket.*;
import java.io.IOException;

/**
 * webSocket客户端
 *
 * @author zhangbin.
 * @date 2017/12/15.
 */
@ClientEndpoint(
        configurator = SampleConfigurator.class,
        decoders = {SampleDecoder.class},
        encoders = {SimpleEncoder.class},
        subprotocols = {"subprotocol1"})
public class WsClient {

    private Logger logger = LoggerFactory.getLogger(WsClient.class);
    private Session session;

    @OnOpen
    public void open(Session session){
        logger.info("Client WebSocket is opening...");
        this.session = session;
    }

    @OnMessage
    public void onMessage(String message){
        logger.info("Server send message: " + message);
    }

    @OnClose
    public void onClose(){
        logger.info("Websocket closed");
    }


    @OnError
    public void onError(Session session, Throwable t) {
        t.printStackTrace();
    }

    public void send(String message){
        this.session.getAsyncRemote().sendText(message);
    }

    public void close() throws IOException {
        if(this.session.isOpen()){
            this.session.close();
        }
    }
}
