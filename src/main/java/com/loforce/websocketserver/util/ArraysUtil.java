package com.loforce.websocketserver.util;

import java.util.*;

/**
 * 数组工具类
 *
 * @author zhangbin.
 * @date 2017/12/15.
 */
public class ArraysUtil {

    public static void main(String[] args) {
        Object[] array1 = new Object[]{"1", "2", "3"};
        Object[] array2 = new Object[]{"a", "b", "c"};
        combime2Arrays(array1, array2);
    }

    /**
     * 合并两个数组
     *
     * @param array1 数组1
     * @param array2 数组2
     * @return 返回合并后的数组
     */
    public static Object[] combime2Arrays(Object[] array1, Object[] array2) {
        List<Object> list = new ArrayList<Object>();
        if (ArraysUtil.isNotEmpty(array1)) {
            list.addAll(Arrays.asList(array1));
        }
        if (ArraysUtil.isNotEmpty(array2)) {
            list.addAll(Arrays.asList(array2));
        }
        System.out.println("合并后的数组：" + new ArrayList<Object>(list));
        Object[] result = new Object[list.size()];
        list.toArray(result);
        return result;
    }

    /**
     * 数组是否为空
     *
     * @param array 指定数组
     * @return true不为空，false为空
     */
    public static boolean isNotEmpty(Object[] array) {
        return array != null && array.length > 0 ? true : false;
    }

    /**
     * 集合是否为空
     *
     * @param list 指定集合
     * @return true不为空，false为空
     */
    public static boolean isNotEmpty(List list) {
        return list != null && list.size() > 0 ? true : false;
    }

    /**
     * 集合是否为空
     *
     * @param set 指定集合
     * @return true不为空，false为空
     */
    public static boolean isNotEmpty(Set set) {
        return set != null && set.size() > 0 ? true : false;
    }

    /**
     * 集合是否为空
     *
     * @param map 指定集合
     * @return true不为空，false为空
     */
    public static boolean isNotEmpty(Map map) {
        return map != null && map.size() > 0 ? true : false;
    }

    /**
     * 求两集合的交集
     *
     * @param s1
     * @param s2
     * @return s1和s2的交集
     */
    public static Set<String> mixedSet(Set<String> s1, Set<String> s2) {
        Set<String> result = new HashSet<String>();
        result.addAll(s1);
        result.retainAll(s2);
        return result;
    }

    /**
     * 求两集合的差集
     *
     * @param s1
     * @param s2
     * @return s1比s2多余的数据
     */
    public static Set<String> diffSet(Set<String> s1, Set<String> s2) {
        Set<String> result = new HashSet<String>();
        result.clear();
        result.addAll(s1);
        result.removeAll(s2);
        return result;
    }
}
