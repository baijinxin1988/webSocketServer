package com.loforce.websocketserver.util;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.DeleteMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * HTTP请求处理类
 *
 * @author zhangbin.
 * @date 2017/12/15.
 */
public class HttpUtil {

    /**
     * 解析输入流中的json数据
     *
     * @param request
     * @return
     */
    public static String getRequestContent(HttpServletRequest request) {
        InputStream inputStream = null;
        BufferedInputStream buf = null;
        StringBuffer requestJsonBuffer = null;
        try {
            inputStream = request.getInputStream();
            buf = new BufferedInputStream(inputStream);
            byte[] buffer = new byte[1024];
            requestJsonBuffer = new StringBuffer();
            int a = 0;
            while ((a = buf.read(buffer)) != -1) {
                requestJsonBuffer.append(new String(buffer, 0, a, "UTF-8"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //关闭连接
            if (null != buf) {
                try {
                    buf.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (null != inputStream) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
        return null == requestJsonBuffer ? null : requestJsonBuffer.toString();
    }

    /**
     * HTTP协议GET请求
     *
     * @param httpclient 请求客户端
     * @param url        请求url
     * @param parameter  请求参数
     * @return
     */
    public static String get(HttpClient httpclient, String url, Map<String, String> parameter) {
        try {
            // 创建httpget
            if (ArraysUtil.isNotEmpty(parameter)) {
                url += "?";
                for (Map.Entry entry : parameter.entrySet()) {
                    url += entry.getKey() + "=" + entry.getValue() + "&";
                }
                url = url.substring(0, url.length() - 1);
            }
            GetMethod get = new GetMethod(url);
            System.out.println("executing request " + get.getURI());
            // 执行get请求.
            httpclient.executeMethod(get);
            System.out.println("--------------------------------------");
            // 打印服务器返回的状态
            System.out.println("Response content length: " + get.getStatusLine());
            String result = get.getResponseBodyAsString();
            // 打印响应内容
            System.out.println("Response content: " + result);
            System.out.println("------------------------------------");
            return result;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * HTTP协议POST请求
     *
     * @param httpclient 请求客户端
     * @param url        请求url
     * @param treeMap    表单参数
     * @return
     */
    public static String postForm(HttpClient httpclient, String url, TreeMap<String, String> treeMap) {
        String json = null;
        if (treeMap == null) {
            System.out.println("提交参数集合为空，请检查参数");
            return null;
        }
        PostMethod post = new PostMethod(url);
        //创建参数队列
        List<NameValuePair> list = new ArrayList<NameValuePair>();
        for (Map.Entry<String, String> entry : treeMap.entrySet()) {
            list.add(new NameValuePair(entry.getKey(), entry.getValue()));
        }
        NameValuePair[] nameValuePairs = new NameValuePair[]{};
        nameValuePairs = list.toArray(nameValuePairs);
        post.setRequestBody(nameValuePairs);
        try {
            httpclient.executeMethod(post);
            System.out.println("--------------------------------------");
            json = post.getResponseBodyAsString();
            System.out.println("Response content: " + json);
            System.out.println("--------------------------------------");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return json;
    }

    /**
     * HTTP协议POST请求
     *
     * @param httpclient 请求客户端
     * @param url        请求url
     * @param json       表单参数
     * @return
     */
    public static String postBody(HttpClient httpclient, String url, String json) {
        if (json == null) {
            System.out.println("提交参数为空，请检查参数");
            return null;
        }
        String result = null;
        PostMethod post = new PostMethod(url);
        post.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET, "utf-8");
        post.setRequestBody(json);
        try {
            httpclient.executeMethod(post);
            result = new String(post.getResponseBody(), "utf-8");
            System.out.println("--------------------------------------");
            System.out.println("Response content: " + result);
            System.out.println("--------------------------------------");
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 删除请求
     *
     * @param url
     * @param dataForm
     * @return
     */
    public static String delete(String url, Map<String, Object> dataForm) {
        HttpClient httpClient = new HttpClient();
        DeleteMethod deleteMethod = new DeleteMethod(url);

        List<NameValuePair> data = new ArrayList<NameValuePair>();
        if (dataForm != null) {
            Set<String> keys = dataForm.keySet();
            for (String key : keys) {
                NameValuePair nameValuePair = new NameValuePair(key, (String) dataForm.get(key));
                data.add(nameValuePair);
            }
        }
        deleteMethod.setQueryString(data.toArray(new NameValuePair[0]));
        try {
            int statusCode = httpClient.executeMethod(deleteMethod);
            if (statusCode != HttpStatus.SC_OK) {
                return "Method failed: " + deleteMethod.getStatusLine();
            }

            // Read the response body.
            byte[] responseBody = deleteMethod.getResponseBody();
            // Deal with the response.
            // Use caution: ensure correct character encoding and is not binary data
            return new String(responseBody);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            deleteMethod.releaseConnection();
        }
        return null;
    }

    public static void main(String[] args) {
        String url = "http://192.168.0.133:8080/group/delete";
        Map<String, Object> dataForm = new HashMap<String, Object>();
        dataForm.put("id", "3");
        dataForm.put("flag", "1");
        delete(url, dataForm);
    }
}
