package com.loforce.websocketserver.util;

import com.loforce.websocketserver.common.base.BaseConstants;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.InvalidSessionException;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Web层辅助类
 *
 * @author alicas
 * @version 2017年6月9日
 */
public final class WebUtil {

    /**
     * 保存当前用户的shiro sessionId到缓存集合
     *
     * @param sessionId
     */
    public static final void saveCurrentUser(String sessionId) {
        BaseConstants.ALL_CURRENT_USERS.put(sessionId, sessionId);
    }

    /**
     * 从缓存集合中获取当前登录用户的shiro sessionId
     *
     * @param sessionId
     * @return 存在返回true，不存在返回false
     */
    public static final boolean getCurrentUser(String sessionId) {
        Object object = BaseConstants.ALL_CURRENT_USERS.get(sessionId);
        if (object == null) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * 从缓存集合中移除当前登录用户的shiro sessionId
     *
     * @param sessionId
     */
    public static final void clearCurrentUser(String sessionId) {
        BaseConstants.ALL_CURRENT_USERS.remove(sessionId);
    }

    /**
     * 保存当前用户
     */
    public static final void saveCurrentUser(Object user) {
        setSession(BaseConstants.CURRENT_USER, user);
    }

    /**
     * 保存当前用户
     */
    public static final void saveCurrentUser(HttpServletRequest request, Object user) {
        setSession(request, BaseConstants.CURRENT_USER, user);
    }

    /**
     * 获取当前用户
     */
    public static final Object getCurrentUser() {
        Subject currentUser = SecurityUtils.getSubject();
        if (null != currentUser) {
            try {
                Session session = currentUser.getSession();
                System.out.println("获取的sessionId=" + session.getId());
                if (null != session) {
                    return session.getAttribute(BaseConstants.CURRENT_USER);
                }
            } catch (InvalidSessionException e) {
            }
        }
        return null;
    }

    /**
     * 获取当前用户
     */
    public static final Object getCurrentUser(HttpServletRequest request) {
        try {
            HttpSession session = request.getSession();
            if (null != session) {
                return session.getAttribute(BaseConstants.CURRENT_USER);
            }
        } catch (InvalidSessionException e) {
        }
        return null;
    }

    /**
     * 将一些数据放到ShiroSession中,以便于其它地方使用
     */
    public static final void setSession(Object key, Object value) {
        Subject currentUser = SecurityUtils.getSubject();
        if (null != currentUser) {
            Session session = currentUser.getSession();
            if (null != session) {
                session.setAttribute(key, value);
            }
        }
    }

    /**
     * 将一些数据放到ShiroSession中,以便于其它地方使用
     */
    public static final void setSession(HttpServletRequest request, String key, Object value) {
        HttpSession session = request.getSession();
        if (null != session) {
            session.setAttribute(key, value);
        }
    }

    /**
     * 移除当前用户
     */
    public static final void removeCurrentUser(HttpServletRequest request) {
        request.getSession().removeAttribute(BaseConstants.CURRENT_USER);
    }


    /**
     * 获取客户端IP
     */
    public static final String getHost(HttpServletRequest request) {
        String ip = request.getHeader("X-Forwarded-For");
        if (StringUtils.isBlank(ip) || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (StringUtils.isBlank(ip) || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (StringUtils.isBlank(ip) || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("X-Real-IP");
        }
        if (StringUtils.isBlank(ip) || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        if ("127.0.0.1".equals(ip)) {
            InetAddress inet = null;
            try { // 根据网卡取本机配置的IP
                inet = InetAddress.getLocalHost();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
            ip = inet.getHostAddress();
        }
        // 对于通过多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割
        if (ip != null && ip.length() > 15) {
            if (ip.indexOf(",") > 0) {
                ip = ip.substring(0, ip.indexOf(","));
            }
        }
        return ip;
    }
}
