package com.loforce.websocketserver.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loforce.websocketserver.constant.MessageConstant;
import com.loforce.websocketserver.server.WsServerPool;

import java.util.LinkedHashMap;

/**
 * webSocket数据工具类
 *
 * @author zhangbin.
 * @date 2017/12/28.
 */
public class WebSocketDataUtil {

    /**
     * webSocket请求数据解析
     *
     * @param text
     * @return
     */
    public static LinkedHashMap<String, String> parseRequestData(String text) {
        LinkedHashMap<String, String> map = null;
        JSONObject object = null;
        try {
            object = JSON.parseObject(text);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (object != null) {
            map = new LinkedHashMap();
            if (text.contains("USER_ON_LINE") || text.contains("HEART_BEAT") || text.contains("ON_LINE_USER_COUNT") || text.contains("ON_LINE_USER_LIST")) {
                map.put("type", object.getString("type"));
                map.put("fromUser", object.getString("fromUser"));
            } else if (text.contains("USER_CHAT")) {
                map.put("type", object.getString("type"));
                map.put("fromUser", object.getString("fromUser"));
                map.put("toUser", object.getString("toUser"));
                map.put("msg", object.getString("msg"));
            } else if (text.contains("GROUP_CHAT")) {
                map.put("type", object.getString("type"));
                map.put("fromUser", object.getString("fromUser"));
                map.put("toGroup", object.getString("toGroup"));
                map.put("groupId", object.getString("groupId"));
                map.put("msg", object.getString("msg"));
            } else if (text.contains("NOTICE_USER_TO_REPORT_COORDINATE_POINT") || text.contains("REFRESH_GROUP_LIST")) {
                map.put("type", object.getString("type"));
                map.put("fromUser", object.getString("fromUser"));
                map.put("toGroup", object.getString("toGroup"));
            }
        }
        return map;
    }


    /**
     * 打包webSocket响应数据包
     *
     * @param type
     * @param fromUser
     * @param toUser
     * @param toGroup
     * @param groupId
     * @param msg
     * @return
     */
    public static String packageResponeData(String type, String fromUser, String toUser, String toGroup, String groupId, String msg) {
        LinkedHashMap map = new LinkedHashMap();
        map.put("type", MessageConstant.USER_CHAT);
        map.put("fromUser", fromUser);
        if (type.equals("HEART_BEAT")) {

        } else if (type.equals("ON_LINE_USER_COUNT")) {
            map.put("onLineUsersCount", WsServerPool.onLineUsersCount());
        } else if (type.equals("ON_LINE_USER_LIST")) {
            map.put("onLineUsersList", WsServerPool.onlineUsers.keySet());
        } else if (type.equals("USER_ON_LINE") || type.equals("USER_OUT_LINE")) {
            map.put("onLineUsersCount", WsServerPool.onLineUsersCount());
            map.put("onLineUserList", WsServerPool.onLineUserList());
        } else if (type.equals("NOTICE_USER_TO_REPORT_COORDINATE_POINT_SUCCESS")) {
            map.put("toGroup", toGroup);
        } else if (type.equals("REFRESH_GROUP_LIST")) {

        } else if (type.equals("USER_CHAT")) {
            map.put("toUser", toUser);
            map.put("msg", msg);
        } else if (type.equals("GROUP_CHAT")) {
            map.put("toGroup", toGroup);
            map.put("groupId", groupId);
            map.put("msg", msg);
        } else if (type.equals("NOTICE_USER_TO_REPORT_COORDINATE_POINT") || type.equals("USER_REPORT_COORDINATE_POINT_SUCCESS")) {
            map.put("toUser", toGroup);
        }
        return JSONObject.toJSONString(map);
    }
}
