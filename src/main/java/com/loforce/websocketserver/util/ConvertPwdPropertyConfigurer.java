package com.loforce.websocketserver.util;

import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;


public class ConvertPwdPropertyConfigurer extends PropertyPlaceholderConfigurer {

    private static String PRO_USERNAME = "username";
    private static String PRO_PASSWORD = "password";

    @Override
    protected String convertProperty(String propertyName, String propertyValue) {
        if (PRO_USERNAME.equals(propertyName)) {
            return AESUtil.AESDecode(propertyValue);
        }
        if (PRO_PASSWORD.equals(propertyName)) {
            return AESUtil.AESDecode(propertyValue);
        }
        return super.convertProperty(propertyName, propertyValue);
    }
}
