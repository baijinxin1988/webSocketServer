package com.loforce.websocketserver.task;

import com.loforce.websocketserver.server.WsServer;

/**
 * 线程任务
 *
 * @author zhangbin.
 * @date 2017/12/18.
 */
public class ChatTask implements Runnable {

    private String sessionId;
    private WsServer wsServer;
    private String toSendMsg;

    public ChatTask(String sessionId, WsServer wsServer, String toSendMsg) {
        this.sessionId = sessionId;
        this.wsServer = wsServer;
        this.toSendMsg = toSendMsg;
    }

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    public void run() {
        wsServer.send(sessionId, toSendMsg);
    }
}
