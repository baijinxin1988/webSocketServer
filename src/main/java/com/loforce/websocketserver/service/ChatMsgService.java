package com.loforce.websocketserver.service;

import com.loforce.websocketserver.common.base.BaseService;
import com.loforce.websocketserver.model.ChatMsg;
import com.loforce.websocketserver.model.ChatMsgExample;

/**
* ChatMsgService接口
* Created by shuzheng on 2017/12/22.
*/
public interface ChatMsgService extends BaseService<ChatMsg, ChatMsgExample> {

}