package com.loforce.websocketserver.service.impl;

import com.loforce.websocketserver.common.annotation.BaseService;
import com.loforce.websocketserver.common.base.BaseServiceImpl;
import com.loforce.websocketserver.dao.def.ChatMsgMapper;
import com.loforce.websocketserver.model.ChatMsg;
import com.loforce.websocketserver.model.ChatMsgExample;
import com.loforce.websocketserver.service.ChatMsgService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
* ChatMsgService实现
* Created by shuzheng on 2017/12/22.
*/
@Service
@Transactional
@BaseService
public class ChatMsgServiceImpl extends BaseServiceImpl<ChatMsgMapper, ChatMsg, ChatMsgExample> implements ChatMsgService {

    private static Logger _log = LoggerFactory.getLogger(ChatMsgServiceImpl.class);

    @Autowired
    ChatMsgMapper chatMsgMapper;

}