package com.loforce.websocketserver.controller;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.loforce.websocketserver.common.base.BaseController;
import com.loforce.websocketserver.constant.HttpApi;
import com.loforce.websocketserver.constant.HttpStatusConstant;
import com.loforce.websocketserver.dto.UserDto;
import com.loforce.websocketserver.server.HttpClientPool;
import com.loforce.websocketserver.util.HttpUtil;
import org.apache.commons.httpclient.HttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author zhangbin.
 * @date 2017/12/21.
 */
@Controller
public class LoginController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(LoginController.class);

    /**
     * 登录
     *
     * @param username
     * @param password
     * @return
     */
    @RequestMapping(value = "/login")
    public String login(String username, String password, ModelMap modelMap, HttpSession session) {
        modelMap.put("username", username);
        JSONObject object = new JSONObject();
        object.put("username", username);
        object.put("password", password);
        object.put("loginClient", 2);
        HttpClient httpclient = new HttpClient();
        String result = HttpUtil.postBody(httpclient, HttpApi.BASE + HttpApi.LOGIN, object.toJSONString());
        object = JSONObject.parseObject(result);
        if (object.getInteger("status").intValue() == HttpStatusConstant.OK.intValue()) {
            String data = object.getString("data");
            JSONObject dataObj = JSONObject.parseObject(data);
            String user = dataObj.getString("user");
            try {
                UserDto userDto = new ObjectMapper().readValue(user, UserDto.class);
                session.setAttribute(username, userDto);
                HttpClientPool.add(username, httpclient);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return "redirect:index";
        } else {
            modelMap.put("msg", object.getString("message"));
            return "login";
        }
    }

    /**
     * 注销
     *
     * @return
     */
    @RequestMapping(value = "/logout")
    public String logout(String username, HttpSession session) {
        UserDto userDto = (UserDto) session.getAttribute(username);
        HttpClient httpClient = HttpClientPool.get(userDto.getUsername());
        String result = HttpUtil.postBody(httpClient, HttpApi.BASE + HttpApi.LOGOUT, "");
        JSONObject object = JSONObject.parseObject(result);
        if (object.getInteger("status").intValue() == HttpStatusConstant.OK.intValue()) {
            return "login";
        } else {
            return "redirect:index";
        }
    }
}
