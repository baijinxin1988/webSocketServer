package com.loforce.websocketserver.controller;

import com.loforce.websocketserver.client.WsClient;
import com.loforce.websocketserver.common.JsonResult;
import com.loforce.websocketserver.common.annotation.MediaType;
import com.loforce.websocketserver.constant.HttpStatusConstant;
import com.loforce.websocketserver.server.WsServerPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.websocket.ContainerProvider;
import javax.websocket.DeploymentException;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * webSocketClient API接口
 *
 * @author zhangbin.
 * @date 2017/12/17.
 */
@Controller
@RequestMapping(value = "/ws/client")
public class WsClientController {

    private Logger logger = LoggerFactory.getLogger(WsClientController.class);
    @Value("#{webSocketConfig.URL}")
    private String url;

    /**
     * 新建webSocket连接
     *
     * @return
     */
    @RequestMapping(value = "/connect", method = RequestMethod.POST, produces = MediaType.APPLICATION_ATOM_XML_VALUE)
    @ResponseBody
    public JsonResult connect() {
        WebSocketContainer conmtainer = ContainerProvider.getWebSocketContainer();
        WsClient wsClient = new WsClient();
        try {
            conmtainer.connectToServer(wsClient, new URI(url));
            return new JsonResult(HttpStatusConstant.OK, "webSocket连接成功");
        } catch (DeploymentException e) {
            e.printStackTrace();
            logger.error("webSocket连接失败：{}", e.getMessage());
            return new JsonResult(HttpStatusConstant.INTERNAL_SERVER_ERROR, "webSocket连接失败：" + e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
            logger.error("webSocket连接失败：{}", e.getMessage());
            return new JsonResult(HttpStatusConstant.INTERNAL_SERVER_ERROR, "webSocket连接失败：" + e.getMessage());
        } catch (URISyntaxException e) {
            e.printStackTrace();
            logger.error("webSocket连接失败：{}", e.getMessage());
            return new JsonResult(HttpStatusConstant.INTERNAL_SERVER_ERROR, "webSocket连接失败：" + e.getMessage());
        }
    }

    /**
     * 发送消息
     *
     * @param username
     * @return
     */
    public JsonResult send(String username) {
        Session session = WsServerPool.onlineUsers.get(username);
        session.getAsyncRemote();
        session.getAsyncRemote();
        return null;
    }
}
