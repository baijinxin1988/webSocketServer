package com.loforce.websocketserver.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.loforce.websocketserver.common.base.BaseController;
import com.loforce.websocketserver.constant.HttpApi;
import com.loforce.websocketserver.constant.HttpStatusConstant;
import com.loforce.websocketserver.dto.MessageGroupDto;
import com.loforce.websocketserver.dto.UserDto;
import com.loforce.websocketserver.server.HttpClientPool;
import com.loforce.websocketserver.server.WsServerPool;
import com.loforce.websocketserver.util.ArraysUtil;
import com.loforce.websocketserver.util.HttpUtil;
import org.apache.commons.httpclient.HttpClient;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.*;

/**
 * @author zhangbin.
 * @date 2017/12/21.
 */
@Controller
public class IndexController extends BaseController {

    @RequestMapping(value = "/index")
    public String index(String username, ModelMap modelMap, HttpServletRequest request, HttpSession session) {
        modelMap.put("serverPath", "http://" + request.getServerName() + ":" + request.getServerPort() + "/");
        modelMap.put("onLineUsersCount", WsServerPool.onLineUsersCount());
        modelMap.put("username", username);
        UserDto userDto = (UserDto) session.getAttribute(username);
        Map<String, String> parameter = new HashMap<>();
        parameter.put("userId", String.valueOf(userDto.getId()));
        HttpClient httpClient = HttpClientPool.get(username);
        String result = HttpUtil.get(httpClient, HttpApi.BASE + HttpApi.MESSAGE_GROUP_LIST, parameter);
        JSONObject object = JSONObject.parseObject(result);
        List<MessageGroupDto> msgGroupList = new ArrayList<>();
        if (object.getInteger("status").intValue() == HttpStatusConstant.OK.intValue()) {
            JSONArray array = object.getJSONArray("data");
            msgGroupList = array.toJavaList(MessageGroupDto.class);
            if(ArraysUtil.isNotEmpty(msgGroupList)){
                Iterator<MessageGroupDto> iterator=msgGroupList.iterator();
                while (iterator.hasNext()){
                    MessageGroupDto messageGroupDto=iterator.next();
                    List<UserDto> userDtos=messageGroupDto.getUserDtoList();
                    if(ArraysUtil.isNotEmpty(userDtos)){
                        String toGroup="";
                        for(UserDto user:userDtos){
                            toGroup+=user.getUsername()+",";
                        }
                        toGroup=toGroup.substring(0,toGroup.length()-1);
                        messageGroupDto.setToGroup(toGroup);
                    }
                }
            }
        }
        modelMap.put("msgGroupList", msgGroupList);
        return "index";
    }
}
