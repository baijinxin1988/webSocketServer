package com.loforce.websocketserver.server;


import com.loforce.websocketserver.util.ArraysUtil;

import javax.websocket.Session;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * webSocket在线用户池
 *
 * @author zhangbin.
 * @date 2017/12/15.
 */
public class WsServerPool {

    /**
     * 在线用户
     */
    public static Map<String, Session> onlineUsers = new ConcurrentHashMap<String, Session>();

    /**
     * 将在线用户加入在线用户列表
     *
     * @param sessionId
     * @param session
     */
    public static synchronized void addUser(String sessionId, Session session) {
        onlineUsers.put(sessionId, session);
    }

    /**
     * 将在线用户从在线用户列表移除
     *
     * @param sessionId
     */
    public static synchronized void removeUser(String sessionId) {
        onlineUsers.remove(sessionId);
    }

    /**
     * 清空所有用户
     */
    public static synchronized void clearUser() {
        onlineUsers.clear();
    }

    /**
     * 在线用户数量
     *
     * @return
     */
    public static synchronized int onLineUsersCount() {
        return onlineUsers.size();
    }

    /**
     * 获取在线用户列表
     *
     * @return
     */
    public static synchronized List<String> onLineUserList() {
        Set<String> list = onlineUsers.keySet();
        List<String> result = new ArrayList<String>();
        if (ArraysUtil.isNotEmpty(list)) {
            Iterator<String> iterator = list.iterator();
            while (iterator.hasNext()) {
                String user = iterator.next();
                user = user.replace("session_id_", "");
                result.add(user);
            }
        }

        Collections.sort(result, new Comparator<String>() {
            public int compare(String o1, String o2) {
                return o1.compareToIgnoreCase(o2) > 0 ? 1 : -1;
            }
        });
        return result;
    }
}
