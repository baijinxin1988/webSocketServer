package com.loforce.websocketserver.server;

import com.alibaba.fastjson.JSONObject;
import com.loforce.websocketserver.model.ChatMsg;
import com.loforce.websocketserver.model.ChatMsgExample;
import com.loforce.websocketserver.service.ChatMsgService;
import com.loforce.websocketserver.task.ChatTask;
import com.loforce.websocketserver.task.TaskThreadPoolExecutor;
import com.loforce.websocketserver.util.ArraysUtil;
import com.loforce.websocketserver.wsservice.MessageService;
import com.loforce.websocketserver.wsservice.MessageServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.ContextLoader;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * webSocket服务端
 *
 * @author zhangbin.
 * @date 2017/12/15.
 */
@ServerEndpoint("/server/{username}")
public class WsServer {

    private Logger logger = LoggerFactory.getLogger(WsServer.class);
    private String sessionIdPrefix = "session_id_";
    private MessageService messageService = new MessageServiceImpl();
    private ThreadPoolExecutor executor;
    @Autowired
    private ChatMsgService chatMsgService = ContextLoader.getCurrentWebApplicationContext().getBean(ChatMsgService.class);

    @OnMessage
    public void onMessage(@PathParam(value = "username") String username, String message) {
        //打印server接收到的message
        String sessionId = getSessionId(username);
        logger.info("消息来自sessionId为: {}的客户端", sessionId);
        logger.info("客户端发来的消息为: {}", message);

        //解析数据
        String type = null;
        String fromUser = username;
        String toUser = null;
        String groupId = null;
        String toGroup = "";
        String msg = "";
        try {
            JSONObject object = JSONObject.parseObject(message);
            type = object.getString("type");
            toUser = object.getString("toUser");
            toGroup = object.getString("toGroup");
            groupId = object.getString("groupId");
            msg = object.getString("msg");
        } catch (Exception e) {
            e.printStackTrace();
            StringBuffer sb = new StringBuffer();
            sb.append("客户端发送的数据【").append(message).append("】无法解析");
            msg = sb.toString();
            logger.error("客户端发送的数据解析数据异常: {}", e.getMessage());
        }
        //server发送消息给client
        response(type, sessionId, fromUser, toUser, groupId, toGroup, msg);
    }

    @OnOpen
    public void onOpen(@PathParam(value = "username") String username, Session session) {
        String sessionId = getSessionId(username);
        if (WsServerPool.onlineUsers.get(sessionId) == null) {
            WsServerPool.onlineUsers.put(sessionId, session);
        }
        //将消息表中的未读消息，推送给用户
        sendUnReadMsgToUser(username, sessionId);
        logger.info("webSocket客户端已连接，sessionId: {}", sessionId);
    }

    @OnClose
    public void onClose(@PathParam(value = "username") String username) {
        String sessionId = getSessionId(username);
        WsServerPool.onlineUsers.remove(sessionId);
        logger.info("webSocket客户端连接关闭，sessionId：{}", sessionId);
        String toSendMsg = messageService.system("USER_OUT_LINE", username, "");
        sendToAll(toSendMsg);
    }

    /**
     * 生成sessionId字符串
     *
     * @param username
     * @return
     */

    private String getSessionId(String username) {
        StringBuffer sessionId = new StringBuffer();
        sessionId.append(sessionIdPrefix).append(username);
        return sessionId.toString();
    }

    /**
     * 给单个用户发送消息
     *
     * @param sessionId
     * @param message
     */
    public void send(String sessionId, String message) {
        try {
            Session session = WsServerPool.onlineUsers.get(sessionId);
            synchronized (session) {
                session.getBasicRemote().sendText(message);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 给在线全体用户发送消息
     *
     * @param msg
     */
    public void sendToAll(String msg) {
        for (Map.Entry entry : WsServerPool.onlineUsers.entrySet()) {
            chatTaskExecute((String) entry.getKey(), msg);
        }
    }

    /**
     * 判断用户是否在线
     *
     * @param sessionId
     * @return 在线为true
     */
    private boolean isOnline(String sessionId) {
        Session session = WsServerPool.onlineUsers.get(sessionId);
        return session != null ? true : false;
    }

    /**
     * 线程执行任务
     *
     * @param toUser
     * @param toSendMsg
     */
    private void chatTaskExecute(String toUser, String toSendMsg) {
        ChatTask chatTask = new ChatTask(toUser, this, toSendMsg);
        executor = TaskThreadPoolExecutor.getInstance();
        executor.execute(chatTask);
    }

    /**
     * 将未读消息存入数据库
     *
     * @param fromUser 消息发送方
     * @param toUser   消息接收方(一对一聊天)
     * @param toGroup  消息接收方(群组聊天)
     * @param groupId  群组ID
     * @param type     消息类型
     * @param msg      消息内容
     */
    private synchronized void insertChatMsg(String fromUser, String toUser, String toGroup, String groupId, String type, String msg) {
        ChatMsg chatMsg = new ChatMsg();
        chatMsg.setFromUser(fromUser);
        chatMsg.setToUser(toUser);
        chatMsg.setToGroup(toGroup);
        chatMsg.setGroupId(Integer.valueOf(groupId));
        chatMsg.setType(type);
        chatMsg.setMsg(msg);
        chatMsg.setCreateDate(new Date());
        chatMsgService.insertSelective(chatMsg);
        logger.info("用户不在线，将消息存入数据库");
    }

    /**
     * 用户上线，推送所有的未读消息
     *
     * @param username
     * @param sessionId
     */
    private void sendUnReadMsgToUser(String username, String sessionId) {
        ChatMsgExample chatMsgExample = new ChatMsgExample();
        chatMsgExample.createCriteria()
                .andToUserEqualTo(username);
        String toSendMsg = messageService.system("USER_ON_LINE", username, "");
        sendToAll(toSendMsg);
        List<ChatMsg> chatMsgList = chatMsgService.selectByExample(chatMsgExample);
        if (ArraysUtil.isNotEmpty(chatMsgList)) {
            Iterator<ChatMsg> iterator = chatMsgList.iterator();
            while (iterator.hasNext()) {
                ChatMsg chatMsg = iterator.next();
                if (isOnline(sessionId)) {
                    chatTaskExecute(sessionId, messageService.chat(chatMsg.getFromUser(), chatMsg.getToUser(), chatMsg.getToGroup(), String.valueOf(chatMsg.getGroupId()), chatMsg.getType(), chatMsg.getMsg()));
                }
            }
            //删除数据库中的未读消息
            chatMsgService.deleteByExample(chatMsgExample);
        }
    }

    /**
     * 服务器响应信息
     *
     * @param type      消息类型
     * @param sessionId 客户端sessionId
     * @param fromUser  一对一聊天，发送方用户sessionId
     * @param toUser    一对一聊天，接收方用户sessionId
     * @param groupId   群组聊天，群组ID
     * @param toGroup   群组聊天，接收用户sessionId字符串，逗号分隔
     * @param msg       消息内容
     *                  (备注：用户上报坐标点时，消息内容为坐标点的字符串拼接，以逗号分隔，例子：121.304498,31.197953)
     */
    private void response(String type, String sessionId, String fromUser, String toUser, String groupId, String toGroup, String msg) {
        if (type == null) {
            chatTaskExecute(sessionId, msg);
        } else {
            StringBuffer targetSessionId;
            //存活心跳(发送时间间隔为10s)
            if (type.equals("HEART_BEAT")) {
                chatTaskExecute(sessionId, messageService.system(type, fromUser, ""));
            }
            //在线用户数量
            else if (type.equals("ON_LINE_USER_COUNT")) {
                chatTaskExecute(sessionId, messageService.system(type, fromUser, ""));
            }
            //在线用户列表
            else if (type.equals("ON_LINE_USER_LIST")) {
                chatTaskExecute(sessionId, messageService.system(type, fromUser, ""));
            }
            //用户一对一聊天
            else if (type.equals("USER_CHAT")) {
                String toSendMsg;
                targetSessionId = new StringBuffer();
                targetSessionId.append(sessionIdPrefix).append(toUser);
                if (isOnline(targetSessionId.toString())) {
                    toSendMsg = messageService.chat(fromUser, toUser, toGroup, groupId, type, msg);
                    chatTaskExecute(targetSessionId.toString(), toSendMsg);
                } else {
                    // 用户不在线，将消息存入数据库,当用户上线的时候，发送给用户
                    insertChatMsg(fromUser, toUser, toGroup, groupId, type, msg);
                }
            }
            //群组聊天
            else if (type.equals("GROUP_CHAT")) {
                if (StringUtils.isNotBlank(toGroup)) {
                    String[] toUsers = toGroup.split(",");
                    String toSendMsg;
                    for (int i = 0; i < toUsers.length; i++) {
                        toUser = toUsers[i];
                        targetSessionId = new StringBuffer();
                        targetSessionId.append(sessionIdPrefix).append(toUser);
                        if (isOnline(targetSessionId.toString())) {
                            toSendMsg = messageService.chat(fromUser, toUser, toGroup, groupId, type, msg);
                            chatTaskExecute(targetSessionId.toString(), toSendMsg);
                        } else {
                            // 用户不在线，将消息存入数据库,当用户上线的时候，发送给用户
                            insertChatMsg(fromUser, toUser, toGroup, groupId, type, msg);
                        }
                    }
                }
            }
            //通知用户上报坐标点
            else if (type.equals("NOTICE_USER_TO_REPORT_COORDINATE_POINT")) {
                String[] toUsers = toGroup.split(",");
                for (int i = 0; i < toUsers.length; i++) {
                    toUser = toUsers[i];
                    targetSessionId = new StringBuffer();
                    targetSessionId.append(sessionIdPrefix).append(toUser);
                    if (isOnline(targetSessionId.toString())) {
                        msg = messageService.noticeUserToReportCoordinatePoint(fromUser, toGroup);
                        chatTaskExecute(targetSessionId.toString(), msg);
                    }
                }
                //发送成功回调信息
                msg = messageService.system("NOTICE_USER_TO_REPORT_COORDINATE_POINT_SUCCESS", fromUser, toGroup);
                chatTaskExecute(sessionId, msg);
            }
            /*//用户上报坐标点成功
            else if(type.equals("USER_REPORT_COORDINATE_POINT_SUCCESS")){
                targetSessionId = new StringBuffer();
                targetSessionId.append(sessionIdPrefix).append(toUser);
                String[] point = msg.split(",");
                String longitude = point[0];
                String latitude = point[1];
                msg = messageService.userReportCoordinatePointSuccess(sessionId, targetSessionId.toString(), longitude, latitude);
                if (isOnline(targetSessionId.toString())) {
                    chatTaskExecute(targetSessionId.toString(), msg);
                }
            }*/
            //刷新用户分组列表
            else if (type.equals("REFRESH_GROUP_LIST")) {
                String[] toUsers = toGroup.split(",");
                for (int i = 0; i < toUsers.length; i++) {
                    toUser = toUsers[i];
                    targetSessionId = new StringBuffer();
                    targetSessionId.append(sessionIdPrefix).append(toUser);
                    if (isOnline(targetSessionId.toString())) {
                        msg = messageService.system(type, fromUser, "");
                        chatTaskExecute(targetSessionId.toString(), msg);
                    }
                }
            }
        }
    }
}
