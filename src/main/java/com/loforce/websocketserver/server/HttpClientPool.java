package com.loforce.websocketserver.server;

import org.apache.commons.httpclient.HttpClient;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 登录用户httpClient缓存池
 *
 * @author zhangbin.
 * @date 2017/12/25.
 */
public class HttpClientPool {

    private static final String CLIENT = "HTTP_CLIENT";
    /**
     * 在线用户
     */
    public static Map<String, HttpClient> pools = new ConcurrentHashMap<String, HttpClient>();

    /**
     * 将在线用户加入在线用户列表
     *
     * @param username
     * @param httpClient
     */
    public static synchronized void add(String username, HttpClient httpClient) {
        pools.put(username, httpClient);
    }

    /**
     * 将在线用户从在线用户列表移除
     *
     * @param username
     */
    public static synchronized void remove(String username) {
        pools.remove(username);
    }

    /**
     * 清空所有用户
     */
    public static synchronized void clear() {
        pools.clear();
    }

    /**
     * 根据key获取HttpClient
     *
     * @param username
     * @return
     */
    public static HttpClient get(String username) {
        return pools.get(username);
    }
}
