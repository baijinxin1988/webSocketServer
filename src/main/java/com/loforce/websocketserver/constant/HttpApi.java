package com.loforce.websocketserver.constant;

/**
 * @author zhangbin.
 * @date 2017/12/25.
 */
public class HttpApi {

    public static String BASE="http://dev.loforce.cn:9990/";
    /**
     * 登录
     */
    public static String LOGIN="login/webLogin";
    /**
     * 注销
     */
    public static String LOGOUT="login/webOut";
    /**
     * 聊天组列表
     */
    public static String MESSAGE_GROUP_LIST="msgGroup/list";
}
