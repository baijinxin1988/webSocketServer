package com.loforce.websocketserver.constant;

/**
 * 状态码常量
 *
 * @author zhangbin.
 * @date 2017/12/15.
 */
public class HttpStatusConstant {
    /**
     * 请求成功
     */
    public static Integer OK = 200;
    /**
     * 错误的请求
     */
    public static Integer BAD_REQUEST = 400;
    /**
     * 请求要求用户的身份认证
     */
    public static Integer UNAUTHORIZED = 401;
    /**
     * 服务器拒绝的请求
     */
    public static Integer FORBIDDEN = 403;
    /**
     * 资源未找到
     */
    public static Integer NOT_FOUND = 404;
    /**
     * 服务器内部错误
     */
    public static Integer INTERNAL_SERVER_ERROR = 500;
}
