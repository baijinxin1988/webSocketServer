package com.loforce.websocketserver.constant;

/**
 * 消息常量
 *
 * @author zhangbin.
 * @date 2017/12/15.
 */
public enum MessageConstant {
    //存活心跳(发送时间间隔为10s)
    HEART_BEAT,
    //获取在线用户数量
    ON_LINE_USER_COUNT,
    //获取在线用户列表
    ON_LINE_USER_LIST,
    //用户上线
    USER_ON_LINE,
    //用户已下线
    USER_OUT_LINE,
    //一对一聊天
    USER_CHAT,
    //群组聊天
    GROUP_CHAT,
    //通知用户上报坐标点
    NOTICE_USER_TO_REPORT_COORDINATE_POINT,
    //用户上报坐标点成功
    USER_REPORT_COORDINATE_POINT_SUCCESS,
    //刷新分组列表
    REFRESH_GROUP_LIST,
    //通知用户上报坐标点成功的回调
    NOTICE_USER_TO_REPORT_COORDINATE_POINT_SUCCESS
}
