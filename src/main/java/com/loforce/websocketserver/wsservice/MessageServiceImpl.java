package com.loforce.websocketserver.wsservice;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.loforce.websocketserver.constant.MessageConstant;
import com.loforce.websocketserver.server.WsServerPool;

import java.util.LinkedHashMap;

/**
 * 消息实现类
 *
 * @author zhangbin.
 * @date 2017/12/15.
 */
public class MessageServiceImpl implements MessageService {

    ObjectMapper mapper = new ObjectMapper();

    /**
     * 系统消息
     *
     * @param type     消息类型
     * @param fromUser 消息发送方
     * @param toGroup  消息接收方
     * @return
     */
    public String system(String type, String fromUser, String toGroup) {
        LinkedHashMap data = new LinkedHashMap();
        data.put("type", type);
        data.put("fromUser", fromUser);
        if (type.equals("HEART_BEAT")) {

        } else if (type.equals("ON_LINE_USER_COUNT")) {
            data.put("onLineUsersCount", WsServerPool.onLineUsersCount());
        } else if (type.equals("ON_LINE_USER_LIST")) {
            data.put("onLineUsersList", WsServerPool.onlineUsers.keySet());
        } else if (type.equals("USER_ON_LINE") || type.equals("USER_OUT_LINE")) {
            data.put("onLineUsersCount", WsServerPool.onLineUsersCount());
            data.put("onLineUserList", WsServerPool.onLineUserList());
        } else if (type.equals("NOTICE_USER_TO_REPORT_COORDINATE_POINT_SUCCESS")) {
            data.put("toGroup", toGroup);
        } else if (type.equals("REFRESH_GROUP_LIST")) {

        }
        try {
            return mapper.writeValueAsString(data);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * 聊天消息
     *
     * @param fromUser 发送方
     * @param toUser   接收方（一对一聊天）
     * @param toGroup  接收方（群组聊天）
     * @param groupId  群组ID
     * @param type     聊天类型
     * @param msg      消息内容
     * @return
     */
    public String chat(String fromUser, String toUser, String toGroup, String groupId, String type, String msg) {
        LinkedHashMap data = new LinkedHashMap();
        data.put("type", MessageConstant.USER_CHAT);
        data.put("fromUser", fromUser);
        data.put("msg", msg);
        if ("USER_CHAT".equals(type)) {
            data.put("toUser", toUser);
        } else {
            data.put("toGroup", toGroup);
            data.put("groupId", groupId);
        }

        try {
            return mapper.writeValueAsString(data);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * 通知用户上报坐标点
     *
     * @param fromUser 发送方
     * @param toUser   接收方
     * @return
     */
    public String noticeUserToReportCoordinatePoint(String fromUser, String toUser) {
        LinkedHashMap data = new LinkedHashMap();
        data.put("type", MessageConstant.NOTICE_USER_TO_REPORT_COORDINATE_POINT);
        data.put("fromUser", fromUser);
        data.put("toUser", toUser);
        try {
            return mapper.writeValueAsString(data);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * 用户上报坐标点成功
     *
     * @param fromUser 发送方
     * @param toGroup  接收方
     * @return
     */
    public String userReportCoordinatePointSuccess(String fromUser, String toGroup) {
        LinkedHashMap data = new LinkedHashMap();
        data.put("type", MessageConstant.USER_REPORT_COORDINATE_POINT_SUCCESS);
        data.put("fromUser", fromUser);
        data.put("toUser", toGroup);
        try {
            return mapper.writeValueAsString(data);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return "";
        }
    }
}
