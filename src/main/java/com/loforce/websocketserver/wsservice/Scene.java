package com.loforce.websocketserver.wsservice;

import com.loforce.websocketserver.model.ChatMsg;
import com.loforce.websocketserver.netty.websocket.WebSocketServerHandler;
import com.loforce.websocketserver.service.ChatMsgService;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.group.ChannelGroup;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import org.apache.commons.lang.StringUtils;

import java.util.Date;
import java.util.LinkedHashMap;

/**
 * 场景处理业务类
 *
 * @author zhangbin.
 * @date 2017/12/28.
 */
public class Scene {

    static MessageService messageService = new MessageServiceImpl();

    public static void handle(LinkedHashMap<String, String> map, ChannelHandlerContext ctx, ChannelGroup group, WebSocketServerHandler handler, ChatMsgService chatMsgService) {
        String type = map.get("type");
        String fromUser = map.get("fromUser");

        //存活心跳(发送时间间隔为10s)
        boolean heartBeat = type.equals("HEART_BEAT");
        //在线用户数量
        boolean onLineUserCount = type.equals("ON_LINE_USER_COUNT");
        //在线用户列表
        boolean onLineUserList = type.equals("ON_LINE_USER_LIST");
        //用户一对一聊天
        boolean userChat = type.equals("USER_CHAT");
        //群组聊天
        boolean groupChat = type.equals("GROUP_CHAT");
        //通知用户上报坐标点
        boolean noticeUserToReportCoordinatePoint = type.equals("NOTICE_USER_TO_REPORT_COORDINATE_POINT");
        //用户上报坐标点成功
        boolean userReportCoordinatePointSucces = type.equals("USER_REPORT_COORDINATE_POINT_SUCCESS");
        //刷新用户分组列表
        boolean refreshGroupList = type.equals("REFRESH_GROUP_LIST");

        if (heartBeat || onLineUserCount || onLineUserList) {
            TextWebSocketFrame tws = new TextWebSocketFrame(messageService.system(type, fromUser, ""));
            handler.send(ctx, tws);
        } else if (userChat) {
            String toUser = map.get("toUser");
            String toGroup = map.get("toGroup");
            String groupId = map.get("groupId");
            String msg = map.get("msg");
            //todo 判断是否在线
            if (group.contains("")) {
                //TODO 一对一聊天发送消息
                TextWebSocketFrame tws = new TextWebSocketFrame(messageService.chat(fromUser, toUser, "", "", type, msg));
                handler.send(ctx, tws);
            } else {
                // 用户不在线，将消息存入数据库,当用户上线的时候，发送给用户
                insertChatMsg(chatMsgService, fromUser, toUser, "", "", type, msg);
            }
        } else if (groupChat) {
            String toGroup = map.get("toGroup");
            String groupId = map.get("groupId");
            String msg = map.get("msg");
            if (StringUtils.isNotBlank(toGroup)) {
                String[] toUsers = toGroup.split(",");
                String toUser;
                for (int i = 0; i < toUsers.length; i++) {
                    toUser = toUsers[i];
                    //todo 判断是否在线
                    if (group.contains("")) {
                        //TODO 一对一聊天发送消息
                        TextWebSocketFrame tws = new TextWebSocketFrame(messageService.chat(fromUser, toUser, toGroup, groupId, type, msg));
                        handler.send(ctx, tws);
                    } else {
                        // 用户不在线，将消息存入数据库,当用户上线的时候，发送给用户
                        insertChatMsg(chatMsgService, fromUser, toUser, toGroup, groupId, type, msg);
                    }
                }
            }
        } else if (noticeUserToReportCoordinatePoint) {
            String toGroup = map.get("toGroup");
            String[] toUsers = toGroup.split(",");
            String toUser;
            for (int i = 0; i < toUsers.length; i++) {
                toUser = toUsers[i];
                //todo 判断是否在线
                if (group.contains("")) {
                    //TODO 一对一聊天发送消息
                    TextWebSocketFrame tws = new TextWebSocketFrame(messageService.noticeUserToReportCoordinatePoint(fromUser, toGroup));
                    handler.send(ctx, tws);
                }
            }
            //发送成功回调信息
            TextWebSocketFrame tws = new TextWebSocketFrame(messageService.system("NOTICE_USER_TO_REPORT_COORDINATE_POINT_SUCCESS", fromUser, toGroup));
            handler.send(ctx, tws);
        }
            /*
            else if(userReportCoordinatePointSucces){
                targetSessionId = new StringBuffer();
                targetSessionId.append(sessionIdPrefix).append(toUser);
                String[] point = msg.split(",");
                String longitude = point[0];
                String latitude = point[1];
                msg = messageService.userReportCoordinatePointSuccess(sessionId, targetSessionId.toString(), longitude, latitude);
                if (isOnline(targetSessionId.toString())) {
                    chatTaskExecute(targetSessionId.toString(), msg);
                }
            }*/

        else if (refreshGroupList) {
            TextWebSocketFrame tws = new TextWebSocketFrame(messageService.system(type, fromUser, ""));
            handler.sendToGroup(group, tws);
        }
    }

    /**
     * 将未读消息存入数据库
     *
     * @param fromUser 消息发送方
     * @param toUser   消息接收方(一对一聊天)
     * @param toGroup  消息接收方(群组聊天)
     * @param groupId  群组ID
     * @param type     消息类型
     * @param msg      消息内容
     */
    private static synchronized void insertChatMsg(ChatMsgService chatMsgService, String fromUser, String toUser, String toGroup, String groupId, String type, String msg) {
        ChatMsg chatMsg = new ChatMsg();
        chatMsg.setFromUser(fromUser);
        chatMsg.setToUser(toUser);
        chatMsg.setToGroup(toGroup);
        chatMsg.setGroupId(Integer.valueOf(groupId));
        chatMsg.setType(type);
        chatMsg.setMsg(msg);
        chatMsg.setCreateDate(new Date());
        chatMsgService.insertSelective(chatMsg);
    }
}
