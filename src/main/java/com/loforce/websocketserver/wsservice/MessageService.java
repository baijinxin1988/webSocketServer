package com.loforce.websocketserver.wsservice;

/**
 * 消息接口
 *
 * @author zhangbin.
 * @date 2017/12/15.
 */
public interface MessageService {

    /**
     * 系统消息
     *
     * @param type     消息类型
     * @param fromUser 消息发送方
     * @param toGroup  消息接收方
     * @return
     */
    String system(String type, String fromUser, String toGroup);

    /**
     * 聊天消息
     *
     * @param fromUser 发送方
     * @param toUser   接收方（一对一聊天）
     * @param toGroup  接收方（群组聊天）
     * @param groupId  群组ID
     * @param type     聊天类型
     * @param msg      消息内容
     * @return
     */
    String chat(String fromUser, String toUser, String toGroup, String groupId, String type, String msg);

    /**
     * 通知用户上报坐标点
     *
     * @param fromUser 发送方
     * @param toUser   接收方
     * @return
     */
    String noticeUserToReportCoordinatePoint(String fromUser, String toUser);

    /**
     * 用户上报坐标点成功
     *
     * @param fromUser 发送方
     * @param toGroup  接收方
     * @return
     */
    String userReportCoordinatePointSuccess(String fromUser, String toGroup);
}
