package com.loforce.websocketserver.dto;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhangbin on 2017/11/16.
 */
public class UserPositionDto {

    private Integer userId;
    private String username;
    private String sipAccount;
    private String photo;
    private String address;
    private String longitude;
    private String latitude;
    private List<PositionInfoDto> PositionInfoDtoList = new ArrayList<>();

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public String getSipAccount() {
        return sipAccount;
    }

    public void setSipAccount(String sipAccount) {
        this.sipAccount = sipAccount;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public List<PositionInfoDto> getPositionInfoDtoList() {
        return PositionInfoDtoList;
    }

    public void setPositionInfoDtoList(List<PositionInfoDto> positionInfoDtoList) {
        PositionInfoDtoList = positionInfoDtoList;
    }
}
