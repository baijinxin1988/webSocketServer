package com.loforce.websocketserver.dto;

import java.io.Serializable;

public class UserInfo implements Serializable {
    /**
     * 主键ID
     *
     * @mbg.generated
     */
    private Integer id;

    /**
     * sys_user表主键ID
     *
     * @mbg.generated
     */
    private Integer userId;

    /**
     * 用户名
     *
     * @mbg.generated
     */
    private String sipLoginName;

    /**
     * 用户密码
     *
     * @mbg.generated
     */
    private String sipLoginPwd;

    /**
     * 所属用户组名称（SIP话机时必填）
     *
     * @mbg.generated
     */
    private String groupName;

    /**
     * 用户等级（SIP接口所有返回值中等级Level均为0-5范围，对应1-6等级）
     *
     * @mbg.generated
     */
    private Integer level;

    /**
     * voip属性（json格式）
     *
     * @mbg.generated
     */
    private String voipAttributes;

    /**
     * 类型（SIP/VOIP二选一）
     *
     * @mbg.generated
     */
    private String type;

    /**
     * 是否允许调度台登录
     *
     * @mbg.generated
     */
    private String scheduler;

    /**
     * 是否只显示本组
     *
     * @mbg.generated
     */
    private String showMore;

    /**
     * 录音
     *
     * @mbg.generated
     */
    private String autoRec;

    /**
     * 视频
     *
     * @mbg.generated
     */
    private String video;

    /**
     * 语音
     *
     * @mbg.generated
     */
    private String audio;

    /**
     * 录像
     *
     * @mbg.generated
     */
    private String rVideo;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getSipLoginName() {
        return sipLoginName;
    }

    public void setSipLoginName(String sipLoginName) {
        this.sipLoginName = sipLoginName;
    }

    public String getSipLoginPwd() {
        return sipLoginPwd;
    }

    public void setSipLoginPwd(String sipLoginPwd) {
        this.sipLoginPwd = sipLoginPwd;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getVoipAttributes() {
        return voipAttributes;
    }

    public void setVoipAttributes(String voipAttributes) {
        this.voipAttributes = voipAttributes;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getScheduler() {
        return scheduler;
    }

    public void setScheduler(String scheduler) {
        this.scheduler = scheduler;
    }

    public String getShowMore() {
        return showMore;
    }

    public void setShowMore(String showMore) {
        this.showMore = showMore;
    }

    public String getAutoRec() {
        return autoRec;
    }

    public void setAutoRec(String autoRec) {
        this.autoRec = autoRec;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public String getrVideo() {
        return rVideo;
    }

    public void setrVideo(String rVideo) {
        this.rVideo = rVideo;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", userId=").append(userId);
        sb.append(", sipLoginName=").append(sipLoginName);
        sb.append(", sipLoginPwd=").append(sipLoginPwd);
        sb.append(", groupName=").append(groupName);
        sb.append(", level=").append(level);
        sb.append(", voipAttributes=").append(voipAttributes);
        sb.append(", type=").append(type);
        sb.append(", scheduler=").append(scheduler);
        sb.append(", showMore=").append(showMore);
        sb.append(", autoRec=").append(autoRec);
        sb.append(", video=").append(video);
        sb.append(", audio=").append(audio);
        sb.append(", rVideo=").append(rVideo);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        UserInfo other = (UserInfo) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()))
            && (this.getSipLoginName() == null ? other.getSipLoginName() == null : this.getSipLoginName().equals(other.getSipLoginName()))
            && (this.getSipLoginPwd() == null ? other.getSipLoginPwd() == null : this.getSipLoginPwd().equals(other.getSipLoginPwd()))
            && (this.getGroupName() == null ? other.getGroupName() == null : this.getGroupName().equals(other.getGroupName()))
            && (this.getLevel() == null ? other.getLevel() == null : this.getLevel().equals(other.getLevel()))
            && (this.getVoipAttributes() == null ? other.getVoipAttributes() == null : this.getVoipAttributes().equals(other.getVoipAttributes()))
            && (this.getType() == null ? other.getType() == null : this.getType().equals(other.getType()))
            && (this.getScheduler() == null ? other.getScheduler() == null : this.getScheduler().equals(other.getScheduler()))
            && (this.getShowMore() == null ? other.getShowMore() == null : this.getShowMore().equals(other.getShowMore()))
            && (this.getAutoRec() == null ? other.getAutoRec() == null : this.getAutoRec().equals(other.getAutoRec()))
            && (this.getVideo() == null ? other.getVideo() == null : this.getVideo().equals(other.getVideo()))
            && (this.getAudio() == null ? other.getAudio() == null : this.getAudio().equals(other.getAudio()))
            && (this.getrVideo() == null ? other.getrVideo() == null : this.getrVideo().equals(other.getrVideo()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
        result = prime * result + ((getSipLoginName() == null) ? 0 : getSipLoginName().hashCode());
        result = prime * result + ((getSipLoginPwd() == null) ? 0 : getSipLoginPwd().hashCode());
        result = prime * result + ((getGroupName() == null) ? 0 : getGroupName().hashCode());
        result = prime * result + ((getLevel() == null) ? 0 : getLevel().hashCode());
        result = prime * result + ((getVoipAttributes() == null) ? 0 : getVoipAttributes().hashCode());
        result = prime * result + ((getType() == null) ? 0 : getType().hashCode());
        result = prime * result + ((getScheduler() == null) ? 0 : getScheduler().hashCode());
        result = prime * result + ((getShowMore() == null) ? 0 : getShowMore().hashCode());
        result = prime * result + ((getAutoRec() == null) ? 0 : getAutoRec().hashCode());
        result = prime * result + ((getVideo() == null) ? 0 : getVideo().hashCode());
        result = prime * result + ((getAudio() == null) ? 0 : getAudio().hashCode());
        result = prime * result + ((getrVideo() == null) ? 0 : getrVideo().hashCode());
        return result;
    }
}