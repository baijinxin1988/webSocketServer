package com.loforce.websocketserver.dto;

/**
 * 用户数据传输类
 * Created by zhangbin on 2017/9/25.
 */
public class UserDto extends SysUser {

    private UserInfo userInfo;

    private SysUserDetail sysUserDetail;
    //用户分组（对讲组编号）
    private String groupNumber;
    //用户分组名
    private String groupName;

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public SysUserDetail getSysUserDetail() {
        return sysUserDetail;
    }

    public void setSysUserDetail(SysUserDetail sysUserDetail) {
        this.sysUserDetail = sysUserDetail;
    }

    public String getGroupNumber() {
        return groupNumber;
    }

    public void setGroupNumber(String groupNumber) {
        this.groupNumber = groupNumber;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
}
