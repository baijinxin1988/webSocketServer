package com.loforce.websocketserver.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhangbin on 2017/11/16.
 */
public class MessageGroupDto extends MessageGroup {
    String toGroup;
    List<UserDto> userDtoList = new ArrayList<>();

    public String getToGroup() {
        return toGroup;
    }

    public void setToGroup(String toGroup) {
        this.toGroup = toGroup;
    }

    public List<UserDto> getUserDtoList() {
        return userDtoList;
    }

    public void setUserDtoList(List<UserDto> userDtoList) {
        this.userDtoList = userDtoList;
    }
}
